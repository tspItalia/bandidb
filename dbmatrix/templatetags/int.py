from django.template import Library

register = Library()

@register.filter
def integer( value ):
  return int(value)