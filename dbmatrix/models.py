from django.db import models
from django.db.models import Max
from django.core.exceptions import *
from django.db.models import Q
import itertools

def matrix(nRows,nCols):
    return [[None]*nCols]*nRows
    #return [ [None] * nCols for i in range(nRows) ]

# debug pourpose
def populateTables():
    for self in Dbm_Table.objects.all():
        rows = self.getRows().order_by('pos')
        cols = self.getColumns().order_by('pos')
        nr = self.lastRowPos()
        nc = self.lastColPos()
        cells = matrix(nr,nc)
        for c in cols:
            for r in rows:
                try:
                    cc=Dbm_Cell.objects.filter(row=r).get(column=c)
                    print("already exist: t: %s r: %d c: %d" % ((self.name),r.pos,c.pos))
                except:
                    val="t:%s r:%d c:%d" % ((self.name),r.pos,c.pos)
                    Dbm_Cell(row=r,column=c,value=val).save()
                    print(val)

class Dbm_Table(models.Model):
    name = models.CharField(max_length=200,default="")

    def __unicode__(self):
        return "%s" % self.name

    def __str__(self):
        return self.__unicode__()
    
    def lastRowPos(self):
        lastrow = Dbm_Row.objects.filter(table=self.id).aggregate(Max('pos'))['pos__max']
        if lastrow:
            return lastrow
        else:
            return 0

    def lastColPos(self):
        lastcol = Dbm_Column.objects.filter(table=self.id).aggregate(Max('pos'))['pos__max']
        # lastcol = return Dbm_Column.objects.filter(table=self.id).order_by('pos')[1:].pos
        if lastcol:
            return lastcol
        else:
            return 0

    def getRow(self,pos=None):
        if pos:
            return Dbm_Row.objects.filter(table=self.id).get(pos=pos)
        else:
            return self.getRows()

    def getRows(self):
        return Dbm_Row.objects.filter(table=self.id)

    def getColumn(self,pos=None):
        if pos:
            return Dbm_Column.objects.filter(table=self.id).get(pos=pos)
        else:
            return self.getColumns()

    def getColumns(self):
        return Dbm_Column.objects.filter(table=self.id).order_by('pos')

    def getCell(self,col_pos=None,row_pos=None):
        # map(None,*[iter(rows_qr)]*len(cols))
        if col_pos and row_pos:
            col = self.getColumn(col_pos)
            row = self.getRow(row_pos)
            cell = Dbm_Cell.objects.filter(row=row).get(column=col)
        elif not col_pos:
            col = self.getColumn(col_pos)
            cell = Dbm_Cell.objects.filter(column=col)
        elif not row_pos:
            row = self.getRow(row_pos)
            cell = Dbm_Cell.objects.filter(row=row)
        else:
            raise ValueError()
        return cell

    def addColumn(self,name="",pos=None,updateIfExists=False):
        if not pos:
            pos = self.lastColPos()+1
        try:
            if len(self.getColumn(pos)) == 0:
                raise ObjectDoesNotExist()
            # la colonna esiste
            # se non si vuole aggiornare colonna una esistente
            if not updateIfExists:
                raise ValueError("row already exists")
            # aggiornamento valore
            else:
                c = Dbm_Column.objects.filter(table=self).get(pos=pos)
                c.update(name=name)
        except ObjectDoesNotExist :
            c = Dbm_Column(table=self,name=name,pos=pos)
            c.save()
            for r in self.getRows():
                Dbm_Cell(row=r,column=c).save()
        return pos

    def addRow(self,name="",pos=None,updateIfExists=False):
        if not pos:
            pos = self.lastRowPos()+1
        try:
            if len(self.getRow(pos)) == 0:
                raise ObjectDoesNotExist()
            # already exists: 
            # se non si vuole aggiornare colonna una esistente
            if not updateIfExists:
                raise ValueError("row already exists")
            # update
            else:
                p = Dbm_Row.object.filter(table=self).get(pos=pos)
                p.update(name=name)
        # new row
        except ObjectDoesNotExist:
            r = Dbm_Row(table=self, name=name, pos=pos)
            r.save()
            # adding empty cells
            for c in self.getColumns():
                Dbm_Cell(row=r,column=c).save()
            
        return pos
    
    def rmColumn(self,pos):
        pos=int(pos)
        print("finding column...")
        col = Dbm_Column.objects.filter(table=self).get(pos=pos)
        print("done")
        print("finding all cell ... ")
        for cell in Dbm_Cell.objects.filter(column=col):
            print("deleteting cell %d" % cell.id)
            cell.delete()
        last = self.lastColPos()
        # se la colonna non e' l'ultima, rinumera le posizioni
        if pos < last:
            print("not last column")
            for p in range(pos,last):
                print("column %d <- %d" % (p,p+1))
                cl = self.getColumn(p+1)
                cl.pos = p
                cl.save()
        print("deleteting column: %d" % col.id)
        col.delete()
        self.__compactColumn__()
        print("all done!")
        return True

    def rmRow(self,pos):
        pos=int(pos)
        row = Dbm_Row.objects.filter(table=self).get(pos=pos)
        for cell in Dbm_Cell.objects.filter(row=row):
            cell.delete()
        last = self.lastRowPos()
        # se la riga non e' ultima, rinumera le posizioni
        if pos < last:
             for p in range(pos,last):
                rw = self.getRow(p+1)
                rw.pos = p
                rw.save()
        row.delete()
        self.__compactRow__()
        return True

    def __compactColumn__(self):
        iterator= itertools.count()
        iterator.next()
        for col in self.getColumns():
            col.pos = iterator.next()
            col.save()

    def __compactRow__(self):
        iterator= itertools.count()
        iterator.next()
        for row in self.getRows():
            row.pos = iterator.next()
            row.save()

    def rmRowById(self,rid):
        row = Dbm_Row.objects.get( id=rid )
        self.rmRow( row.pos )
        return True

    '''
    def allCells(self):
        rows = self.getRows().order_by('pos')
        cols = self.getColumns().order_by('pos')
        nr = self.lastRowPos()
        nc = self.lastColPos()
        cells = matrix(nr+1,nc+1)
        for c in cols:
            for r in rows:
                try:
                    cells[r.pos][c.pos] = Dbm_Cell.objects.filter(row=r).get(column=c)
                except self.DoesNotExist:
                    pass
        return cells
    '''

    def __allCells__(self):
        return Dbm_Cell.objects.filter(Q(column__table=self) | Q(row__table=self))

    def allCells(self):
        nr = self.lastRowPos()
        nc = self.lastColPos()
        cells_matrix = matrix(nr,nc)
        cells = self.__allCells__()
        for r in range(nr):
            rp = r+1
            cco = cells.filter(row__pos=rp)
            for c in range(nc):
                cp = c+1
                cells_matrix[r][c] = cco.get(column__pos=cp)
        return cells_matrix


    def updateValue(self,row_pos,col_pos,value):
        cell = self.getCell(col_pos=col_pos,row_pos=row_pos)
        cell.value = value
        cell.save()

class Dbm_Column(models.Model):
    table = models.ForeignKey(Dbm_Table)
    pos = models.IntegerField()
    name = models.CharField(max_length=200,default="")
    def __unicode__(self):
        return "%s - %d - %s" % (str(self.table), self.pos, self.name)
    def __str__(self):
        return self.__unicode__()
    
class Dbm_Row(models.Model):
    table = models.ForeignKey(Dbm_Table)
    pos = models.IntegerField()
    name = models.CharField(max_length=200,default="")
    def __unicode__(self):
        return "%s - %d - %s" % (str(self.table), self.pos, self.name)
    def __str__(self):
        return self.__unicode__()

class Dbm_Cell(models.Model):
    row = models.ForeignKey(Dbm_Row)
    column = models.ForeignKey(Dbm_Column)
    value = models.CharField(max_length=200,default="",blank=True)
    def __unicode__(self):
        #return "%d - %d - %s" % (self.column.pos, self.row.pos, self.value)
        return self.value
    def __str__(self):
        return self.__unicode__()



