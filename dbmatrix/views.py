from django.shortcuts import render,render_to_response
from django.shortcuts import redirect
from dbmatrix.models import *
from django.core.exceptions import *
from dbmatrix.forms import *
from django.db.models import Q
from django.forms import HiddenInput
from django.http import Http404
from django.template import RequestContext

def failed_req(table_id,message=None):
    raise Http404("table: %d: %s" % (int(table_id),message) )

def success_req(table_id):
    return redirect(edit,permanent=False,table_id=table_id)

def getFormset(tid):
    #q=(Q(column__table__id=tid) | Q(row__table__id=tid))
    queryset=Dbm_Cell.objects.filter(Q(column__table__id=tid) , Q(row__table__id=tid))
    formset = TableFormset(queryset=queryset)
    return formset

'''
def getFormset(tid):
    TableFormset = modelformset_factory(Dbm_Cell, formset=MatrixForm)
'''

def show(request,table_id):
    #rows_qr = Dbm_Row.objects.filter(table=table_id).order_by('pos').order_by('row')
    #cols = Dbm_Column.objects.filter(table=table_id).order_by('pos')
    #rows = map(None,*[iter(rows_qr)]*len(cols))
    #return render_to_response( 'dbm_show.html', { 'cols': cols , 'rows': rows } )
    try:
        table = Dbm_Table.objects.get(id=table_id)
        cells = table.__allCells__()
        dimension = { 'column':table.lastColPos(), 'row':table.lastRowPos() }
        return render_to_response('dbm_show.html',{'cells':cells,'dimension' : dimension})
    except Dbm_Table.DoesNotExist as e:
        return failed_req(int(table_id),e.message)

class TableIterator(object):
    __count__ = 0
    def __init__(self,values):
        if type(values) != list:
            raise KeyError("agument must be a list")
        values.sort()
        values.reverse()
        self.values = values

    def next(self):
        try:
            return self.values.pop()
        except IndexError:
            pass

    def current(self):
        try:
            return self.values[-1]
        except IndexError:
            pass

def __getColIterator__(table):
    colpos = [ col.pos for col in table.getColumns() ]
    return TableIterator(colpos)

def __getRowIterator__(table):
    rowpos = [ row.pos for row in table.getRows() ]
    return TableIterator(rowpos)

def __validate_n_save__(post,table_id):
    queryset=Dbm_Cell.objects.filter(Q(column__table__id=table_id) , Q(row__table__id=table_id))
    formset = TableFormset(post,queryset=queryset)
    if formset.is_valid():
        formset.save()
        #print("formset saved")
        return True,formset
    else:
        for form in formset:
            if form.is_valid():
                #print("form %s valid" % form.prefix)
                form.save()
        '''
            else:
                print("form %s not valid:" % form.prefix)
                for e in form.errors:
                    print("\t %s " % e)
        print("formset error: %s" % formset.get_form_error())
        for e in formset.errors:
            print("\t %s" % e)
        '''
        return False,formset

def edit(request,table_id):
    try:
        table = Dbm_Table.objects.get(id=table_id)
    except Dbm_Table.DoesNotExist:
        return failed_req(int(table_id))
    if request.method == 'POST':
        result , formset = __validate_n_save__(request.POST,table_id)
    else:
        formset = getFormset(table_id)
        result = False
    dimension = { 'column':table.lastColPos(), 'row':table.lastRowPos() }
    row_iter = __getRowIterator__(table)
    col_iter = __getColIterator__(table)
    return render(
            request,
            'dbm_edit.html',
            {   
                'valid'     : result ,
                'table_id'  : int(table_id) ,
                'forms'     : formset ,
                'dimension' : dimension ,
                'row_iter'  : row_iter ,
                'col_iter'  : col_iter ,
            },
            context_instance = RequestContext(request)
        )

def new(request):
    table = Dbm_Table()
    table.save()
    return redirect(edit,permanent=False,table_id=table.id)

def add_col(request,table_id):
    try:
        table = Dbm_Table.objects.get(id=table_id)
        if request.method == 'POST':
            __validate_n_save__(request.POST,table_id)
        if table.addColumn():
            return success_req(table_id)
        else:
            return failed_req(table_id)
    except Dbm_Table.DoesNotExist:
        return failed_req(table_id)


def add_row(request,table_id):
    try:
        table = Dbm_Table.objects.get(id=table_id)
        if request.method == 'POST':
            __validate_n_save__(request.POST,table_id)
        if table.addRow():
            return success_req(table_id)
        else:
            return failed_req(table_id)
    except Dbm_Table.DoesNotExist:
        return failed_req(table_id)

def rm_row(request,table_id,row):
    row = int(row)
    try:
        table = Dbm_Table.objects.get(id=table_id)
        if request.method == 'POST':
            __validate_n_save__(request.POST,table_id)
        table.rmRow(row)
        return success_req(table_id)
    except (Dbm_Table.DoesNotExist,Dbm_Row.DoesNotExist) as e:
        return failed_req(table_id,e.message)

def rm_row_id(request,table_id,row):
    table_id = int(table_id)
    if request.method == 'POST':
        __validate_n_save__(request.POST,table_id)
    row = int(row)
    try:
        table = Dbm_Table.objects.get(id=table_id)
        table.rmRowById(row)
        return success_req(table_id)
    except (Dbm_Table.DoesNotExist,Dbm_Row.DoesNotExist) as e:
        return failed_req(table_id,e.message)


def rm_column(request,table_id,column_pos):
    try:
        table = Dbm_Table.objects.get( id=table_id )
        if request.method == 'POST':
            __validate_n_save__(request.POST,table_id)
        table.rmColumn(column_pos)
        return success_req(table_id)
    except (Dbm_Table.DoesNotExist,Dbm_Column.DoesNotExist) as e:
        return failed_req( table_id,e.message )
    
