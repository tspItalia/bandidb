from dbmatrix.models import *
from django.forms.formsets import formset_factory
from django.forms.models import modelformset_factory
from django.forms import *

TableFormset = modelformset_factory(
		Dbm_Cell,
		widgets = {
			'column': HiddenInput, 
			'row': HiddenInput, 
			},
		extra = 0
	)

'''
class CellForm(BaseModelFormSet):
    column = forms.ModelChoiceField(queryset=Dbm_Column.objects.all(), widget=forms.HiddenInput() )
    row = forms.ModelChoiceField(queryset=Dbm_Row.objects.all(), widget=forms.HiddenInput() )
    value = forms.
    class Meta:
        model = Dbm_Cell

MatrixForm = formset_factory(CellForm)
'''