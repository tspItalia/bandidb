from django.conf.urls import patterns, include, url
from dbmatrix import views
urlpatterns = patterns('dbmatrix.views',
	url(r'new/$',views.new,name='new'),
	url(r'(?P<table_id>\d+)/show/$',views.show,name='show'),
	url(r'(?P<table_id>\d+)/edit/$',views.edit,name='edit'),
	url(r'(?P<table_id>\d+)/add/column/$',views.add_col,name='add_col'),
	url(r'(?P<table_id>\d+)/add/row/$',views.add_row,name="add_row"),
	#url(r'(?P<table_id>\d+)/rm/row/(?P<row>\d)',views.rm_row_id,name="rm_row"),
	url(r'(?P<table_id>\d+)/rm/row/(?P<row>\d)',views.rm_row,name="rm_row"),
	url(r'(?P<table_id>\d+)/rm/column/(?P<column_pos>\d)/$',views.rm_column,name="rm_column"),
)