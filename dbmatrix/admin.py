from django.contrib import admin
from dbmatrix.models import Dbm_Column,Dbm_Table,Dbm_Row,Dbm_Cell

admin.site.register(Dbm_Column)
admin.site.register(Dbm_Table)
admin.site.register(Dbm_Row)
admin.site.register(Dbm_Cell)
