#!/bin/bash
if [ -z "$1" ] ; then
  echo "usage: $0 <APP_NAME>"
  exit 1
fi
APP_NAME="$1"
echo "Please, enter mysql root password"
./manage.py sqlclear $APP_NAME | mysql -h localhost -u root -p bandidb
./manage.py syncdb

