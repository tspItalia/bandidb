from bandiapp.models import *
from django.forms.formsets import formset_factory
from django.forms.models import modelformset_factory
from django.forms import *

BandoFormset = modelformset_factory( Bando )

class BandoForm(ModelForm):
	class Meta:
		model = Bando
		fileds = '__all__'
		labels = {
			'valore' : 'Valore dell\'appalto',
			'oggetto' : 'Oggetto della gara',
			'scadenza' : 'Scadenza presentazione offerta',
			'apertura' : 'Apertura busta',
			'partecipazione' : 'TSP Italia partecipa?',
			'stipula_contratto' : 'Data stipula contratto',
			'scadenza_contratto' : 'Data scadenza contratto',
			'durata' : 'Durata (anni)'
		}