from django.db import models
from dbmatrix.models import Dbm_Table

class Documento(models.Model):
    descrizione = models.CharField(max_length="255")
    file = models.FileField(upload_to="Documenti/%Y/%m/%d")
    
    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.descrizione

class Bando(models.Model):
    comune = models.CharField(max_length=55)
    valore = models.FloatField()
    RISTRETTA = 'rs'
    INDAGINE  = 'in'
    APERTA    = 'ap'
    TYPE_CHOICES = (
        ( RISTRETTA, 'Ristretta' ),
        ( INDAGINE, 'Indagine' ),
        ( APERTA, 'Aperta' ),
    )
    SINGOLA = 'IS'
    RTI     = 'RT'
    TERZI   = 'TR'
    PART_CHOICES = (
        ( SINGOLA, 'Impresa Singola' ),
        ( RTI, 'RTI' ),
        ( TERZI, 'TERZI' ),
    )
    SI = True
    NO = False
    DOUBLE_CHOICES = (
        ( SI, 'SI' ),
        ( NO, 'NO' ),
    )
    tipo = models.CharField( 
            max_length = 2, 
            choices = TYPE_CHOICES, 
            default = APERTA 
        )
    oggetto = models.CharField(
            max_length=255
        )
    stalli = models.PositiveIntegerField()
    scadenza = models.DateField()
    apertura = models.DateField()
    tariffa  = models.TextField(
            max_length = 1000
        )
    periodo = models.TextField(
            max_length = 1000
        )
    durata = models.PositiveIntegerField()
    ausiliari = models.BooleanField(
            default = False
        )
    descrizione = models.TextField(
            max_length = 1000
        )
    partecipazione = models.BooleanField(
            choices = DOUBLE_CHOICES
        )
    tipo_partecipazione = models.CharField( 
            max_length = 2, 
            choices    = PART_CHOICES, 
            blank      = True 
        )
    info_terzi = models.TextField(
            max_length = 1000, 
            blank      = True
        )
    offerta_tecnica = models.OneToOneField(
            Dbm_Table,
            blank = True,
            null = True,
            related_name = 'bando_offerta_tecnica'
        )
    offerta_economica = models.OneToOneField(
            Dbm_Table,
            blank = True,
            null = True,
            related_name = 'bando_offerta_economica'
        )
    aggiudicatario = models.CharField(
            blank = True,
            max_length = 255
        )
    offerta_aggiudicatario = models.TextField(
            blank = True
        )
    stipula_contratto = models.DateField(
            blank = True,
            null = True
        )
    scandenza_contratto = models.DateField(
            blank = True,
            null = True
        )
    documenti = models.ManyToManyField(
            Documento,
            blank = True
        )
    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "%s %s" % (str(self.scadenza.year),str(self.comune) )

    def in_scadenza(self):
        return self.scandenza_contratto >= timezone.now() - datetime.timedelta(months=1)
    in_scadenza.short_description='Prossimo alla scadenza?'