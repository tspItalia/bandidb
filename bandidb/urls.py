from django.conf.urls import patterns, include, url
from dbmatrix import urls as dbmatrix_urls
from bandiapp import urls as bandiapp_urls
from django.contrib import admin
import bandiapp
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^table/', include(dbmatrix_urls)),
    url(r'^new/$', bandiapp.views.insert, name='insert'),
    url(r'^show/(?P<bando_id>\d+)', bandiapp.views.show, name='show'),
)