#!/bin/sh
set -e
./resync_appdb.sh bandiapp
./resync_appdb.sh dbmatrix

./manage.py convert_to_south dbmatrix
./manage.py convert_to_south bandiapp

./manage.py migrate bandiapp
./manage.py migrate dbmatrix
./manage.py migrate django_extensions