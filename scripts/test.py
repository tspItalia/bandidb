from django.forms import ModelForm
from dbmatrix.models import *
from dbmatrix.forms import *
from django import forms
from django.forms.formsets import formset_factory
from django.db.models import Q

def run__old():
    table = Dbm_Table(id=1)
    DbmCellFormSet = formset_factory(CellForm)
    cells = table.allCells()
    init=[]
    for row in cells:
        for cell in row:
            if cell :
                init.append({
                    'column': cell.column,
                    'value': cell.value,
                    'row': cell.row,
                    })
    cfs =  DbmCellFormSet(initial=init)

    for form in cfs:
        for field in form:
            print 


def run():

    t = Dbm_Table.objects.get(id=4)
    ccc = t.allCells()
    i=0
    print("t.allCells() query:")
    for cc in ccc:
        for c in cc:
            if c:
                #print("column table id: %d\nrow table id: %d" % (c.column.table.id,c.row.table.id))
                print(c.id)
                i+=1
    print("total: %d"%i)
    print("Q() query:")
    i=0
    cc=Dbm_Cell.objects.filter(Q(column__table__id=t.id) | Q(row__table__id=t.id))
    for c in cc:
        i+=1
        #print("column table id: %d\nrow table id: %d" % (c.column.table.id,c.row.table.id))
        print(c.id)
        
    print("total: %d"%i)